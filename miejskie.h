#include <iostream>
#include <iomanip>
#include <cmath>
#include <cstdlib>
#include "auto.h"

using namespace std;


class miejskie :public Auto
{
protected:
    int poj_bak=50;
public:
    miejskie(int pal)
    {
        paliwo=pal;
        if(pal>poj_bak)
        {
            paliwo=poj_bak;
            cout << "Ten typ auta nie zmiesci tyle paliwa, auto zatankowane do pelna: " << paliwo << "l" << endl;
            cout << endl;
        }

    }
    virtual void spalanie();
    virtual void podroz();

};
