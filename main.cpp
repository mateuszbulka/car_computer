#include <iostream>
#include <iomanip>
#include <cmath>
#include <cstdlib>
#include "auto.h"
#include "miejskie.h"
#include "wyscigowe.h"
#include "terenowe.h"
#include "ciezarowe.h"

using namespace std;

void parametry()
{
    cout << "Prosze podac startowa liczbe litrow paliwa:" << endl;
}
void typ()
{
    cout << "Prosze wybrac typ samochodu:" << endl;
    cout << "Miejskie - m" << endl;
    cout << "Wyscigowe - w" << endl;
    cout << "Terenowe - t" << endl;
    cout << "Ciezarowe - c" << endl;
}
void menu()
{
    cout << "Witaj w menu programu, co chcesz zrobic:" << endl;
    cout << "Start/Stop silnika - e" << endl;
    cout << "Zapiecie/Odpiecie pasow - f" << endl;
    cout << "Wlaczenie/Wylaczenie swiatel - h" << endl;
    cout << "Tankowanie - l" << endl;
    cout << "Podroz - r" << endl;
    cout << "Ile km na obecnym paliwie - d" << endl;
    cout << "Calkowity przejechany dystans - t" << endl;
    cout << endl;
    cout << "Kliknij (x) by wyjsc z programu" << endl;
}

int main()
{
    cout << "Witamy w programie do analizy typow aut" << endl;
    Auto *wsk;
    float i;
    char wybor,wybor2;
    typ();
    cin >> wybor;
    switch (wybor)
    {
    case 109:
    {
    parametry();
    cin>>i;
    miejskie mi(i);
    wsk = &mi;
    do{
    menu();
    cin >> wybor2;
    switch (wybor2)
    {
        case 100:
        wsk -> spalanie();
        break;
        case 108:
        wsk -> tankowanie();
        break;
        case 114:
        wsk -> podroz();
        break;
        case 116:
        wsk -> calkowity_dystans();
        break;
        case 101:
        wsk -> start_silnika();
        break;
        case 102:
        wsk -> pasy();
        break;
        case 104:
        wsk -> swiatla();
        break;
    }
    }while(wybor2!=120);

    }
    case 119:
    {
    parametry();
    cin>>i;
    wyscigowe wy(i);
    wsk = &wy;
    do{
    menu();
    cin >> wybor2;
    switch (wybor2)
    {
        case 100:
        wsk -> spalanie();
        break;
        case 108:
        wsk -> tankowanie();
        break;
        case 114:
        wsk -> podroz();
        break;
        case 116:
        wsk -> calkowity_dystans();
        break;
        case 101:
        wsk -> start_silnika();
        break;
        case 102:
        wsk -> pasy();
        break;
        case 104:
        wsk -> swiatla();
        break;
    }
    }while(wybor2!=120);

    }
    case 116:
    {
    parametry();
    cin>>i;
    terenowe te(i);
    wsk = &te;
    do{
    menu();
    cin >> wybor2;
    switch (wybor2)
    {
        case 100:
        wsk -> spalanie();
        break;
        case 108:
        wsk -> tankowanie();
        break;
        case 114:
        wsk -> podroz();
        break;
        case 116:
        wsk -> calkowity_dystans();
        break;
        case 101:
        wsk -> start_silnika();
        break;
        case 102:
        wsk -> pasy();
        break;
        case 104:
        wsk -> swiatla();
        break;
    }
    }while(wybor2!=120);

    }
    case 99:
    {
    parametry();
    cin>>i;
    ciezarowe ci(i);
    wsk = &ci;
    do{
    menu();
    cin >> wybor2;
    switch (wybor2)
    {
        case 100:
        wsk -> spalanie();
        break;
        case 108:
        wsk -> tankowanie();
        break;
        case 114:
        wsk -> podroz();
        break;
        case 116:
        wsk -> calkowity_dystans();
        break;
        case 101:
        wsk -> start_silnika();
        break;
        case 102:
        wsk -> pasy();
        break;
        case 104:
        wsk -> swiatla();
        break;
    }
    }while(wybor2!=120);

    }
    }
    return 0;
}
