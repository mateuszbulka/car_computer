#pragma once
#include <iostream>
#include <iomanip>
#include <cmath>
#include <cstdlib>

using namespace std;

class Auto
{
protected:
    int paliwo,dystans,poj_bak,IL,dystans_p,dystans_m=0,dystans_t=0,dystans_c=0,paliwo_ch=0;
    char typ_teren;
    bool start=false,swiat=false,pas=false;
public:
    virtual void spalanie()=0;
    virtual void podroz()=0;
    void tankowanie();
    void start_silnika();
    void swiatla();
    void pasy();
    void calkowity_dystans();
};
