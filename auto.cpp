#include <iostream>
#include <iomanip>
#include <cmath>
#include <cstdlib>
#include "auto.h"

using namespace std;

    void Auto::tankowanie()
    {
        if(start==true)
        {
            cout << "Przed przystapieniem do tankowania prosze o wylaczenie silnika !" << endl;
            cout << endl;
        }
        else
        {
        cout << "Ile paliwa chcialbys zatankowac?" << endl;
        cin >> IL;
        if (IL+paliwo>poj_bak)
        {
            cout << "Tyle paliwa nie zmiesci sie w baku!" << endl;
            cout << "Obecny poziom paliwa: " << paliwo << "l" << endl;
            cout << endl;
        }
        else
        {
            paliwo=(paliwo+IL);
            cout << "Paliwo zatankowane!" << endl;
            cout << "Obecna ilosc paliwa w baku: " << paliwo << "l" << endl;
            cout << endl;
        }
        }
    }
    void Auto::start_silnika()
    {
        if(start==false)
        {
            start=true;
            cout << "Silnik wlaczony!" << endl;
            cout << endl;
        }
        else
        {
            start=false;
            cout << "Silnik wylaczony!" << endl;
            cout << endl;
        }
    }
    void Auto::swiatla()
    {
        if(swiat==false)
        {
            swiat=true;
            cout << "Swiatla wlaczone!" << endl;
            cout << endl;
        }
        else
        {
            swiat=false;
            cout << "Swiatla wylaczone!" << endl;
            cout << endl;
        }
    }
    void Auto::pasy()
    {
        if(pas==false)
        {
            pas=true;
            cout << "Pasy zapiete!" << endl;
        }
        else
        {
            pas=false;
            cout << "Pasy niezapiete!" << endl;
        }
    }
    void Auto::calkowity_dystans()
    {
        cout << "Calkowity dystans jaki przejechales to: " << (dystans_m+dystans_t+dystans_c) << "km" << endl;
        cout << "W miescie: " << (dystans_m) << "km" << endl;
        cout << "W trasie: " << (dystans_t) << "km" << endl;
        cout << "W cyklu mieszanym: " << (dystans_c) << "km" << endl;
        cout << endl;
    }
